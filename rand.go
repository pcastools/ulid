// Rand provides a rand source for ULID generation.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package ulid

import (
	"bufio"
	"encoding/binary"
	"io"
	"math"
	"math/bits"
	"math/rand"
)

// uint80 defines an 80-bit integer (16-bit Hi value, 64-bit Lo value).
type uint80 struct {
	Hi uint16
	Lo uint64
}

// monotonicEntropy is an opaque type that provides monotonic entropy.
type monotonicEntropy struct {
	io.Reader
	ms      uint64
	entropy uint80
	rand    [8]byte
	rng     *rand.Rand
}

// maxRand is an upper bound on the value returned by a monotonicEntropy source.
const maxRand = math.MaxUint32

/////////////////////////////////////////////////////////////////////////
// uint80 functions
/////////////////////////////////////////////////////////////////////////

func (u *uint80) SetBytes(bs []byte) {
	u.Hi = binary.BigEndian.Uint16(bs[:2])
	u.Lo = binary.BigEndian.Uint64(bs[2:])
}

func (u *uint80) AppendTo(bs []byte) {
	binary.BigEndian.PutUint16(bs[:2], u.Hi)
	binary.BigEndian.PutUint64(bs[2:], u.Lo)
}

func (u *uint80) Add(n uint64) (overflow bool) {
	lo, hi := u.Lo, u.Hi
	if u.Lo += n; u.Lo < lo {
		u.Hi++
	}
	return u.Hi < hi
}

func (u uint80) IsZero() bool {
	return u.Hi == 0 && u.Lo == 0
}

/////////////////////////////////////////////////////////////////////////
// monotonicEntropy functions
/////////////////////////////////////////////////////////////////////////

// newMonotonicEntropy returns an entropy source that is guaranteed to yield strictly increasing entropy bytes for the same ULID timestamp. On conflicts, the previous ULID entropy is incremented with a random number between 1 and maxRand (inclusive).
func newMonotonicEntropy(entropy io.Reader) *monotonicEntropy {
	m := &monotonicEntropy{
		Reader: bufio.NewReader(entropy),
	}
	if rng, ok := entropy.(*rand.Rand); ok {
		m.rng = rng
	}
	return m
}

// MonotonicRead implements the MonotonicReader interface.
func (m *monotonicEntropy) MonotonicRead(ms uint64, entropy []byte) (err error) {
	if !m.entropy.IsZero() && m.ms == ms {
		err = m.increment()
		m.entropy.AppendTo(entropy)
	} else if _, err = io.ReadFull(m.Reader, entropy); err == nil {
		m.ms = ms
		m.entropy.SetBytes(entropy)
	}
	return err
}

// increment the previous entropy number with a random number of up to maxRand (inclusive).
func (m *monotonicEntropy) increment() error {
	if inc, err := m.random(); err != nil {
		return err
	} else if m.entropy.Add(inc) {
		return ErrMonotonicOverflow
	}
	return nil
}

// random returns a uniform random value in [1, maxRand), reading entropy from m.Reader.
// Adapted from: https://golang.org/pkg/crypto/rand/#Int
func (m *monotonicEntropy) random() (inc uint64, err error) {
	// Fast path for using a underlying rand.Rand directly.
	if m.rng != nil {
		return 1 + uint64(m.rng.Int63n(maxRand)), nil
	}
	// bitLen is the maximum bit length needed to encode a value < maxRand
	bitLen := bits.Len64(maxRand)
	// byteLen is the maximum byte length needed to encode a value < maxRand
	byteLen := uint(bitLen+7) / 8
	// msbitLen is the number of bits in the most significant byte of maxRand-1
	msbitLen := uint(bitLen % 8)
	if msbitLen == 0 {
		msbitLen = 8
	}
	for inc == 0 || inc >= maxRand {
		if _, err = io.ReadFull(m.Reader, m.rand[:byteLen]); err != nil {
			return 0, err
		}
		// Clear bits in the first byte to increase the probability that the
		// candidate is < maxRand.
		m.rand[0] &= uint8(int(1<<msbitLen) - 1)
		// Convert the read bytes into an uint64 with byteLen
		switch byteLen {
		case 1:
			inc = uint64(m.rand[0])
		case 2:
			inc = uint64(binary.LittleEndian.Uint16(m.rand[:2]))
		case 3, 4:
			inc = uint64(binary.LittleEndian.Uint32(m.rand[:4]))
		case 5, 6, 7, 8:
			inc = binary.LittleEndian.Uint64(m.rand[:8])
		}
	}
	// Range: [1, maxRand)
	return 1 + inc, nil
}
