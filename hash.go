// Hash defines a hash function for ULIDs.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package ulid

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// toUint32 converts an slice of 4 bytes to a uint32.
func toUint32(b []byte) uint32 {
	return uint32(b[0]) + uint32(b[1])<<8 + uint32(b[2])<<16 + uint32(b[3])<<24
}

// combine returns a new hash value given by combining h1 and h2.
func combine(h1 uint32, h2 uint32) uint32 {
	return h1 ^ (h2 + 0x9e3779b9 + (h1 << 6) + (h1 >> 2))
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Hash returns a hash of u.
func (u ULID) Hash() uint32 {
	h := toUint32(u[:4])
	h = combine(h, toUint32(u[4:8]))
	h = combine(h, toUint32(u[8:12]))
	h = combine(h, toUint32(u[12:]))
	return h
}
