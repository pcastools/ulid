// Package ulid implements a Universally Unique Lexicographically Sortable Identifier (ULID).
/*
A ULID is designed to be a drop-in replacement for a UUID. A ULID:
 * is compatible with UUID/GUID's;
 * 1.21e+24 unique ULIDs per millisecond;
 * is lexicographically sortable;
 * canonically encodes as a 26 character string (as opposed to the 36
	character UUID);
 * uses Crockford's base32 for better efficiency and readability - see
	http://www.crockford.com/wrmg/base32.html;
 * is case insensitive;
 * has no special characters (URL safe);
 * has a monotonic sort order (correctly detects and handles the same
	 millisecond).

The canonical spec for a ULID is at:
	https://github.com/ulid/spec

This package is based on oklog's package:
	https://github.com/oklog/ulid
The API has been adjusted slightly to make it similar to:
	https://github.com/satori/go.uuid

*/
package ulid

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

import (
	"bytes"
	"database/sql/driver"
	"math/rand"
	"strconv"
	"sync"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// ulid.go
/////////////////////////////////////////////////////////////////////////

// Error represents an error when manipulating ULID data.
type Error int

// The ULID errors.
const (
	// ErrDataSize is returned when parsing or unmarshaling ULIDs with the wrong
	// data size.
	ErrDataSize = Error(iota)
	// ErrInvalidCharacters is returned when parsing or unmarshaling ULIDs with
	// invalid Base32 encodings.
	ErrInvalidCharacters
	// ErrOverflow is returned when unmarshaling a ULID whose first character is
	// larger than 7, thereby exceeding the valid bit depth of 128.
	ErrOverflow
	// ErrMonotonicOverflow is returned by a Monotonic entropy source when
	// incrementing the previous ULID's entropy bytes would result in overflow.
	ErrMonotonicOverflow
	// ErrScanValue is returned when the value passed to scan cannot be
	// unmarshaled into the ULID.
	ErrScanValue
)

/*
ULID is a 16 byte Universally Unique Lexicographically Sortable Identifier.

	The components are encoded as 16 octets.
	Each component is encoded with the MSB first (network byte order).

	0                   1                   2                   3
	0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	|                      32_bit_uint_time_high                    |
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	|     16_bit_uint_time_low      |       16_bit_uint_random      |
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	|                       32_bit_uint_random                      |
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	|                       32_bit_uint_random                      |
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
type ULID [16]byte

// EncodedSize is the length of a text encoded ULID.
const EncodedSize = 26

// Encoding is the base 32 encoding alphabet used in ULID strings.
const Encoding = "0123456789ABCDEFGHJKMNPQRSTVWXYZ"

// Byte to index table for O(1) lookups when unmarshaling. We use 0xFF as sentinel value for invalid indexes.
var dec = [...]byte{
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x01,
	0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E,
	0x0F, 0x10, 0x11, 0xFF, 0x12, 0x13, 0xFF, 0x14, 0x15, 0xFF,
	0x16, 0x17, 0x18, 0x19, 0x1A, 0xFF, 0x1B, 0x1C, 0x1D, 0x1E,
	0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x0A, 0x0B, 0x0C,
	0x0D, 0x0E, 0x0F, 0x10, 0x11, 0xFF, 0x12, 0x13, 0xFF, 0x14,
	0x15, 0xFF, 0x16, 0x17, 0x18, 0x19, 0x1A, 0xFF, 0x1B, 0x1C,
	0x1D, 0x1E, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
}

// Nil is special form of ULID that is specified to have all 128 bits set to zero.
var Nil = ULID{}

// maxTime is the maximum time that can be represented in an ULID.
var maxTime = ULID{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}.Time()

// The source of entropy (with protecting mutex)
var (
	randM   sync.Mutex
	randSrc = newMonotonicEntropy(rand.New(rand.NewSource(time.Now().UnixNano())))
)

// NullULID can be used with the standard sql package to represent a ULID value that can be NULL in the database
type NullULID struct {
	ULID  ULID
	Valid bool
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// timestamp converts a time.Time to Unix milliseconds. Because of the way ULID stores time, times from the year 10889 produces undefined results.
func timestamp(t time.Time) uint64 {
	t = t.UTC()
	return uint64(t.Unix())*1000 +
		uint64(t.Nanosecond()/int(time.Millisecond))
}

/////////////////////////////////////////////////////////////////////////
// Error functions
/////////////////////////////////////////////////////////////////////////

// Error returns the error message for the given ULID error.
func (e Error) Error() string {
	switch e {
	case ErrDataSize:
		return "bad data size when unmarshaling"
	case ErrInvalidCharacters:
		return "bad data characters when unmarshaling"
	case ErrOverflow:
		return "overflow when unmarshaling"
	case ErrMonotonicOverflow:
		return "monotonic entropy overflow"
	case ErrScanValue:
		return "source value must be a string or byte slice"
	default:
		return "unknown error code [" + strconv.Itoa(int(e)) + "]"
	}
}

/////////////////////////////////////////////////////////////////////////
// ULID functions
/////////////////////////////////////////////////////////////////////////

// generateULID generates a new ULID.
func generateULID() (ULID, error) {
	var u ULID
	// Acquire a lock
	randM.Lock()
	defer randM.Unlock()
	// Set the time component
	ms := timestamp(time.Now())
	u[0] = byte(ms >> 40)
	u[1] = byte(ms >> 32)
	u[2] = byte(ms >> 24)
	u[3] = byte(ms >> 16)
	u[4] = byte(ms >> 8)
	u[5] = byte(ms)
	// Generate the entropy
	err := randSrc.MonotonicRead(ms, u[6:])
	if err != nil {
		return ULID{}, err
	}
	// Return the ULID
	return u, nil
}

// New returns a new ULID.
func New() (ULID, error) {
	u, err := generateULID()
	for i := 0; i < 100 && err != nil; i++ {
		// We retry 100 times; the probability that we don't successfully
		// acquire a ULID is vanishingly small
		time.Sleep(time.Millisecond)
		u, err = generateULID()
	}
	return u, err
}

// parse attempts to parse the given byte slice into the given ULID.
func parse(v []byte, u *ULID) error {
	// Check if a base32 encoded ULID is the right length.
	if len(v) != EncodedSize {
		return ErrDataSize
	}
	// Check if all the characters in a base32 encoded ULID are part of the
	// expected base32 character set.
	if dec[v[0]] == 0xFF || dec[v[1]] == 0xFF || dec[v[2]] == 0xFF ||
		dec[v[3]] == 0xFF || dec[v[4]] == 0xFF || dec[v[5]] == 0xFF ||
		dec[v[6]] == 0xFF || dec[v[7]] == 0xFF || dec[v[8]] == 0xFF ||
		dec[v[9]] == 0xFF || dec[v[10]] == 0xFF || dec[v[11]] == 0xFF ||
		dec[v[12]] == 0xFF || dec[v[13]] == 0xFF || dec[v[14]] == 0xFF ||
		dec[v[15]] == 0xFF || dec[v[16]] == 0xFF || dec[v[17]] == 0xFF ||
		dec[v[18]] == 0xFF || dec[v[19]] == 0xFF || dec[v[20]] == 0xFF ||
		dec[v[21]] == 0xFF || dec[v[22]] == 0xFF || dec[v[23]] == 0xFF ||
		dec[v[24]] == 0xFF || dec[v[25]] == 0xFF {
		return ErrInvalidCharacters
	}
	// Check if the first character in a base32 encoded ULID will overflow. This
	// happens because the base32 representation encodes 130 bits, while the
	// ULID is only 128 bits.
	if v[0] > '7' {
		return ErrOverflow
	}
	// 6 bytes timestamp (48 bits)
	(*u)[0] = (dec[v[0]] << 5) | dec[v[1]]
	(*u)[1] = (dec[v[2]] << 3) | (dec[v[3]] >> 2)
	(*u)[2] = (dec[v[3]] << 6) | (dec[v[4]] << 1) | (dec[v[5]] >> 4)
	(*u)[3] = (dec[v[5]] << 4) | (dec[v[6]] >> 1)
	(*u)[4] = (dec[v[6]] << 7) | (dec[v[7]] << 2) | (dec[v[8]] >> 3)
	(*u)[5] = (dec[v[8]] << 5) | dec[v[9]]
	// 10 bytes of entropy (80 bits)
	(*u)[6] = (dec[v[10]] << 3) | (dec[v[11]] >> 2)
	(*u)[7] = (dec[v[11]] << 6) | (dec[v[12]] << 1) | (dec[v[13]] >> 4)
	(*u)[8] = (dec[v[13]] << 4) | (dec[v[14]] >> 1)
	(*u)[9] = (dec[v[14]] << 7) | (dec[v[15]] << 2) | (dec[v[16]] >> 3)
	(*u)[10] = (dec[v[16]] << 5) | dec[v[17]]
	(*u)[11] = (dec[v[18]] << 3) | dec[v[19]]>>2
	(*u)[12] = (dec[v[19]] << 6) | (dec[v[20]] << 1) | (dec[v[21]] >> 4)
	(*u)[13] = (dec[v[21]] << 4) | (dec[v[22]] >> 1)
	(*u)[14] = (dec[v[22]] << 7) | (dec[v[23]] << 2) | (dec[v[24]] >> 3)
	(*u)[15] = (dec[v[24]] << 5) | dec[v[25]]
	return nil
}

// FromString returns a ULID parsed from string input. Input is expected in a form accepted by UnmarshalText.
func FromString(ulid string) (ULID, error) {
	var u ULID
	return u, parse([]byte(ulid), &u)
}

// FromStringOrNil returns a ULID parsed from string input, or the Nil ULID on error.
func FromStringOrNil(ulid string) ULID {
	u, err := FromString(ulid)
	if err != nil {
		return Nil
	}
	return u
}

// FromBytes returns a ULID converted from raw byte slice input. It will return error if the slice isn't 16 bytes long.
func FromBytes(input []byte) (ULID, error) {
	var u ULID
	return u, u.UnmarshalBinary(input)
}

// FromBytesOrNil returns a ULID converted from raw byte slice input, or the Nil ULID on error.
func FromBytesOrNil(input []byte) ULID {
	u, err := FromBytes(input)
	if err != nil {
		return Nil
	}
	return u
}

// Must is a helper that wraps a call to a function returning (ULID, error) and panics if the error is non-nil. It is intended for use in variable initialisations such as
//	var packageULID = ulid.Must(ulid.FromString("01DQ7W1MG7282JXKCWM0T3FE3R"))
func Must(u ULID, err error) ULID {
	if err != nil {
		panic(err)
	}
	return u
}

// Compare returns an integer comparing u and v lexicographically. The result will be 0 if u == v, -1 if u < v, and +1 if u > v.
func Compare(u ULID, v ULID) int {
	return bytes.Compare(u[:], v[:])
}

// Equal returns true iff u equals v equals.
func Equal(u ULID, v ULID) bool {
	return bytes.Equal(u[:], v[:])
}

// Bytes returns bytes slice representation of u.
func (u ULID) Bytes() []byte {
	return u[:]
}

// Entropy returns the entropy part of u.
func (u ULID) Entropy() []byte {
	e := make([]byte, 10)
	copy(e, u[6:])
	return e
}

// Time returns the time part of u (in millisecond precision).
func (u ULID) Time() time.Time {
	ms := uint64(u[5]) | uint64(u[4])<<8 | uint64(u[3])<<16 |
		uint64(u[2])<<24 | uint64(u[1])<<32 | uint64(u[0])<<40
	s := int64(ms / 1e3)
	ns := int64((ms % 1e3) * 1e6)
	return time.Unix(s, ns)
}

// String returns a lexicographically sortable string encoded ULID (26 characters, non-standard base 32), e.g. 01AN4Z07BY79KA1307SR9X4MV3. The format is "tttttttttteeeeeeeeeeeeeeee" where 't' is time and 'e' is entropy.
func (u ULID) String() string {
	ulid, _ := u.MarshalText()
	return string(ulid)
}

// MarshalBinary implements the encoding.BinaryMarshaler interface by returning u as a byte slice.
func (u ULID) MarshalBinary() ([]byte, error) {
	ulid := make([]byte, len(u))
	copy(ulid, u[:])
	return ulid, nil
}

// UnmarshalBinary implements the encoding.BinaryUnmarshaler interface by copying the passed data and converting it to a ULID.
func (u *ULID) UnmarshalBinary(data []byte) error {
	if len(data) != len(*u) {
		return ErrDataSize
	}
	copy((*u)[:], data)
	return nil
}

// MarshalText implements the encoding.TextMarshaler interface by returning u encoded as a string.
func (u ULID) MarshalText() ([]byte, error) {
	ulid := make([]byte, EncodedSize)
	// 10 byte timestamp
	ulid[0] = Encoding[(u[0]&224)>>5]
	ulid[1] = Encoding[u[0]&31]
	ulid[2] = Encoding[(u[1]&248)>>3]
	ulid[3] = Encoding[((u[1]&7)<<2)|((u[2]&192)>>6)]
	ulid[4] = Encoding[(u[2]&62)>>1]
	ulid[5] = Encoding[((u[2]&1)<<4)|((u[3]&240)>>4)]
	ulid[6] = Encoding[((u[3]&15)<<1)|((u[4]&128)>>7)]
	ulid[7] = Encoding[(u[4]&124)>>2]
	ulid[8] = Encoding[((u[4]&3)<<3)|((u[5]&224)>>5)]
	ulid[9] = Encoding[u[5]&31]
	// 16 bytes of entropy
	ulid[10] = Encoding[(u[6]&248)>>3]
	ulid[11] = Encoding[((u[6]&7)<<2)|((u[7]&192)>>6)]
	ulid[12] = Encoding[(u[7]&62)>>1]
	ulid[13] = Encoding[((u[7]&1)<<4)|((u[8]&240)>>4)]
	ulid[14] = Encoding[((u[8]&15)<<1)|((u[9]&128)>>7)]
	ulid[15] = Encoding[(u[9]&124)>>2]
	ulid[16] = Encoding[((u[9]&3)<<3)|((u[10]&224)>>5)]
	ulid[17] = Encoding[u[10]&31]
	ulid[18] = Encoding[(u[11]&248)>>3]
	ulid[19] = Encoding[((u[11]&7)<<2)|((u[12]&192)>>6)]
	ulid[20] = Encoding[(u[12]&62)>>1]
	ulid[21] = Encoding[((u[12]&1)<<4)|((u[13]&240)>>4)]
	ulid[22] = Encoding[((u[13]&15)<<1)|((u[14]&128)>>7)]
	ulid[23] = Encoding[(u[14]&124)>>2]
	ulid[24] = Encoding[((u[14]&3)<<3)|((u[15]&224)>>5)]
	ulid[25] = Encoding[u[15]&31]
	return ulid, nil
}

// UnmarshalText implements the encoding.TextUnmarshaler interface by parsing the data as string-encoded ULID.
func (u *ULID) UnmarshalText(v []byte) error {
	return parse(v, u)
}

// Scan implements the sql.Scanner interface. It supports scanning a string or byte slice.
func (u *ULID) Scan(src interface{}) error {
	switch x := src.(type) {
	case nil:
		return nil
	case string:
		return u.UnmarshalText([]byte(x))
	case []byte:
		return u.UnmarshalBinary(x)
	}
	return ErrScanValue
}

// Value implements the sql/driver.Valuer interface. This returns the value represented as a byte slice.
func (u ULID) Value() (driver.Value, error) {
	return u.MarshalBinary()
}

// IsNil returns true iff this equals the nil-valued ULID with 128 bits set to zero.
func (u ULID) IsNil() bool {
	return u[0] == 0 && u[1] == 0 && u[2] == 0 &&
		u[3] == 0 && u[4] == 0 && u[5] == 0 &&
		u[6] == 0 && u[7] == 0 && u[8] == 0 &&
		u[9] == 0 && u[10] == 0 && u[11] == 0 &&
		u[12] == 0 && u[13] == 0 && u[14] == 0
}

/////////////////////////////////////////////////////////////////////////
// NullULID functions
/////////////////////////////////////////////////////////////////////////

// Value implements the driver.Valuer interface.
func (u NullULID) Value() (driver.Value, error) {
	if !u.Valid {
		return nil, nil
	}
	// Delegate to ULID Value function
	return u.ULID.Value()
}

// Scan implements the sql.Scanner interface.
func (u *NullULID) Scan(src interface{}) error {
	if src == nil {
		u.ULID, u.Valid = Nil, false
		return nil
	}
	// Delegate to ULID Scan function
	u.Valid = true
	return u.ULID.Scan(src)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// MaxTime returns the maximum time that can be encoded in an ULID.
func MaxTime() time.Time {
	return maxTime
}
